http://jira.cengage.com/browse/MTQ-13435  closed
Answer in range more than +/-2% tolerance is graded as correct

http://jira.cengage.com/browse/MTQ-12947
Feedback pop-up: �Show Correct Answer Initially� mode: Correct Answer(s) is not shown in the pop-up for �V� icon if Submit button has not been clicked before

http://jira.cengage.com/browse/MTQ-12944
Fractions format is not consistent in the problem statement and in the Solution section

http://jira.cengage.com/browse/MTQ-12949
[Mode C] Correct answer is not displayed in the feedback pop-up

http://jira.cengage.com/browse/MTQ-13380   closed
Three significant figures should be displayed in the feedback pop-up / "Show Correct Answer Initially" mode

http://jira.cengage.com/browse/MTQ-13121
Four instead of three significant figures should be retained in intermediate computations 

http://jira.cengage.com/browse/MTQ-13431
Trailing zeros are not needed when exact answer is zero

http://jira.cengage.com/browse/MTQ-13261
Two significant digits should be displayed in the �Show Correct Answer Initially� mode/Feedback pop-up.

http://jira.cengage.com/browse/MTQ-14041
Displaying of formulas is corrupted in essay editor

http://jira.cengage.com/browse/MTQ-13435
Answer in range more than +/-2% tolerance is graded as correct

http://jira.cengage.com/browse/MTQ-14772
�see this table� link in problem statement doesn�t work

http://jira.cengage.com/browse/MTQ-13492
Incorrectly rounded value is displayed in the answer field

http://jira.cengage.com/browse/MTQ-13823
Power value is not in the superscript

http://jira.cengage.com/browse/MTQ-13039
Inappropriate feedback is given if enter letters in the answer field

http://jira.cengage.com/browse/MTQ-13384
Unnecessary boldface is applied to the multiplication symbol

http://jira.cengage.com/browse/MTQ-14201
�Item Rendering Error� message is displayed instead of item�s data

http://jira.cengage.com/browse/MTQ-13036
�Resource not found� error message is displayed in the pop-up window if click on GETTING STARTED and I�M STUCK links 

http://jira.cengage.com/browse/MTQ-14696
The equations should be correctly aligned.