package some_name;

/**
 * Created by andriygorbach on 4/11/2016.
 */
public class class_1 {
    public static void main ( String[] args ) {
        double a = Math.random();
        double b = Math.random();

        System.out.println("initial value for the first variable is " + a);
        System.out.println("initial value for the second variable is " + b);
        System.out.println("---Arithmetic operations---");
        System.out.println(b + a + " - here is an addition example");
        System.out.println(b - a + " - here is a subtraction example");
        System.out.println(b * a + " - here is a multiplication example");
        System.out.println(b / a + " - here is a division example");
        System.out.println(b % a + " - here is a modulus action example");
        System.out.println("---Relational operations---");
        if (b == a) {
            System.out.println("second variable is equal to the first variable");
        }
        if (b != a) {
            System.out.println("second variable is not equal to the first variable");
        }
        if (b > a) {
            System.out.println("second variable is more than the first variable");
        }
        if (b >= a) {
            System.out.println("second variable is more than or equal to the the first variable");
        }
        if (b < a) {
            System.out.println("second variable is less than the first variable");
        }
        if (b <= a) {
            System.out.println("second variable is less than or equal to the first variable");
        }
        System.out.println("---Conditional operations---");

        if ((a<1) && (b<1)) {
            System.out.println("both variables are less than 1");
        }
        if ((a<1) || (b<1)) {
            System.out.println("some variable is less than 1");
        }
        System.out.println("---Assignment operations---");
        System.out.print(a=b);
        System.out.println(" - equality operation example");
        System.out.print(a+=b);
        System.out.println(" - += operation example");
        System.out.print(a-=b);
        System.out.println(" - -= operation example");
        System.out.print(a*=b);
        System.out.println(" - *= operation example");
        System.out.print(a/=b);
        System.out.println(" - /= operation example");
        System.out.print(a%=b);
        System.out.println(" - %= operation example");

    }
}







