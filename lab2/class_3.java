package some_name;

/**
 * Created by andriygorbach on 4/11/2016.
 */
/*
*3) Переведите число с точкой в тип без точки и наоборот - выведите результаты
/*

 */
public class class_3 {

    public static void main ( String[] args ) {

        double b = 1.0;
        float f = 20.0F;
        long long_ = 30L;

        int b1 = (int) b;
        System.out.println(b1 + " is a converted double into integer");
        double b2 = (double) b1;
        System.out.println(b2 + " is a converted integer into double");

        int f1 = (int) f;
        System.out.println(f1 + " is a converted float into integer");
        float f2 = (float) f1;
        System.out.println(f2 + " is a converted integer into float");

        int long_1 = (int) long_;
        System.out.println(long_1 + " is a converted long into integer");
        long long_2 = (long) long_1;
        System.out.println(long_2 + " is a converted integer into long");
    }
}
