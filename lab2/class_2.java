package some_name;

/**
 * Created by andriygorbach on 4/11/2016.
 */

/*
*2) Переведите значение каждого примитивного типа в строку и обратно - выведите результаты на  экран
 */
public class class_2 {

    public static void main ( String[] args ) {


        byte a = 1;
        String a1 = Byte.toString(a);
        System.out.println(a1 + " is a converted byte into string");
        String a2 = String.valueOf(a1);
        System.out.println(a2 + " is a converted string into byte");

        short b = 2;
        String b1 = Short.toString(b);
        System.out.println(b1 + " is a converted short into string");
        String b2 = String.valueOf(b1);
        System.out.println(b2 + " is a converted string into short");

        int c = 3;
        String c1 = Integer.toString(c);
        System.out.println(c1 + " is a converted integer into string");
        String c2 = String.valueOf(c1);
        System.out.println(c2 + " is a converted string into integer");

        long d = 40;
        String d1 = Long.toString(d);
        System.out.println(d1 + " is a converted long into string");
        String d2 = String.valueOf(d1);
        System.out.println(d2 + " is a converted string into long");

        double e = 5.4;
        String e1 = Double.toString(e);
        System.out.println(e1 + " is a converted double into string");
        String e2 = String.valueOf(e1);
        System.out.println(e2 + " is a converted string into double");

        float f = 0.6F;
        String f1 = Float.toString(f);
        System.out.println(f1 + " is a converted float into string");
        String f2 = String.valueOf(f1);
        System.out.println(f2 + " is a converted string into float");

        char g = '\ucfcf';
        String g1 = Character.toString(g);
        System.out.println(g1 + " is a converted char into string");
        String g2 = String.valueOf(g1);
        System.out.println(g2 + " is a converted string into char");

        boolean h = false;
        String h1 = Boolean.toString(h);
        System.out.println(h1 + " is a converted boolean variable into string");
        String h2 = String.valueOf(h1);
        System.out.println(h2 + " is a converted string into boolean variable");


    }
}
