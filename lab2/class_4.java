package some_name;

/**
 * Created by andriygorbach on 4/11/2016.
 */
/*
*4) Написать программу которая бы запросила пользователя ввести число, затем знак математической операции, затем еще число и вывела результат. Должно работать и для целых и для дробных чисел
*/

import java.io.*;

public class class_4 {

    public static BufferedReader q = new BufferedReader(new InputStreamReader(System.in));

    public static void main ( String[] args ) throws IOException {
        try {
            System.out.print("Enter the 1st number: ");
            String num1 = q.readLine();
            double n1 = Double.parseDouble(num1);


            System.out.print("Enter operation: ");
            String op = q.readLine();
            char Q = op.charAt(0);
            if ((Q != '/') && (Q != '*') && (Q != '-') && (Q != '+')) {
                System.out.println("Incorrect operator inserted");
            }

            System.out.print("Enter the 2nd number: ");
            String num2 = q.readLine();
            double n2 = Double.parseDouble(num2);

            if (n2 == 0 && Q == '/') {
                System.out.println("I can't divide on 0");
            } else if (Q == '+') {
                double sum = 0;
                sum = n1 + n2;
                int sum1 = (int) sum;
                if ((sum - sum1 == 0) || (sum - sum1 == 0.0)) {
                    System.out.println("The sum is " + sum1);
                } else {
                    System.out.println("The sum is " + sum);

                }
            } else if (Q == '-') {
                double diff = 0;
                diff = n1 - n2;
                int diff1 = (int) diff;
                if ((diff - diff1 == 0) || (diff - diff1 == 0.0)) {
                    System.out.println("The difference is " + diff1);
                } else {
                    System.out.println("The difference is " + diff);

                }


            } else if (Q == '*') {
                double prod = 0;
                prod = n1 * n2;
                int prod1 = (int) prod;
                if ((prod - prod1 == 0) || (prod - prod1 == 0.0)) {
                    System.out.println("The product is " + prod1);
                } else {
                    System.out.println("The product is " + prod);

                }

            } else if (Q == '/') {
                double quo = 0;
                quo = n1 / n2;
                int quo1 = (int) quo;
                if ((quo - quo1 == 0) || (quo - quo1 == 0.0)) {
                    System.out.println("The quotient is " + quo1);
                } else {
                    System.out.println("The quotient is " + quo);

                }
            }

        } catch (IllegalArgumentException e) {
            System.out.println("Invalid. Please, insert only numbers");
        }
    }
}

