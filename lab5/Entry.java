package lab5;

import java.util.*;

/**
 * Created by andriygorbach on 5/11/2016.
 */
public class Entry implements List {

    List myList = new ArrayList<>();


    @Override
    public int size() {
        int aa = Calendar.MAY;
        return aa;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        Iterator iterator = myList.iterator();
        while(iterator.hasNext()) {
            String element = (String) iterator.next();
        }
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        myList.add("Monday");
        myList.add("Tuesday");
        myList.add("Wednesday");
        myList.add("Thursday");
        myList.add("Friday");
        myList.add("Saturday");
        myList.add("Sunday");
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public void clear() {
        myList.clear();
    }

    @Override
    public Object get(int index) {
        return null;
    }

    @Override
    public Object set(int index, Object element) {
        return null;
    }

    @Override
    public void add(int index, Object element) {

    }

    @Override
    public Object remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }


    private GregorianCalendar firstDate;
    private int workDays = 0;
    private int lastDay;
    private int lastMonth;
    private int lastYear;

    /**
     * � ������������ ��������������� ��� ���� � �������: ������� ����, �����, ��� ��� ������ ���� (����� �������)
     * � ����� ������� ����, �����, ��� ��� ������ ���� (��� ��, ����� �������)
     * ��� ���� �������� � ���������������� ������� (���� ������ � ����� ����������� � 1)
     * ����� �� ��� ���������� ������� (� ����� ���� ��� � ������������ � ���������)
     */
    public Entry(int day1, int month1, int year1, int day2, int month2, int year2) {
        firstDate = new GregorianCalendar(year1, --month1, --day1);
        lastDay = --day2;
        lastMonth = --month2;
        lastYear = year2;

    }

    /**
     * ��������� ������� ���������� ������ ����������� ���� �� ������ ���� (firstDate) �� ������ (���������� last{Day, Month, Year})
     * ������� ��� ����
     */
    public void calculateWorkDays() {
        //���� ��������, ���� ����, �����, ��� ��� ������ ���� ������ ����������� �������� ������ ����
        while (firstDate.get(Calendar.DAY_OF_MONTH) <= lastDay || firstDate.get(Calendar.MONTH) < lastMonth || firstDate.get(Calendar.YEAR) < lastYear) {

            //������� ���� ������� ����� ��  �������
            if (firstDate.get(Calendar.DAY_OF_WEEK) < 6) {
                workDays++;
            }

            //��� ���� ���� ������ (���������� � ������ ���� 1 ����)
            firstDate.add(Calendar.DAY_OF_MONTH, 1);
        }
        System.out.println("���������� ������� ���� = " + workDays);
        System.out.println("���������� �������� ���� = " + (31 - workDays));
    }



    public static void main(String[] args) {
        //������ ���� - 9 �������, ������ - 19 ������
        Entry obj = new Entry(1, 5, 2016, 31, 05, 2016);
        obj.calculateWorkDays();
    }

}
