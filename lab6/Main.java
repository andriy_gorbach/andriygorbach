package lab6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

import static java.lang.System.out;

public class Main {

    public static String path = "D:/JavaFolder/";
    public static boolean etern = true;
    public static String stopBufReader = "stop";


    public static void main(String[] args) throws IOException {
        while (true) {
            String fileName = getFileName();
            if (Objects.equals(fileName, "stop")) {
                return;
            }
            new Thread(new Copier(path, fileName)).start();
        }

    }

    public static String getFileName() {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        out.println("Populate file name:");


        try {
            return bufReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";


    }
}

