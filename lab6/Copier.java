package lab6;

import sun.util.calendar.BaseCalendar;
import sun.util.calendar.LocalGregorianCalendar;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Calendar;

import static java.lang.System.out;

/**
 * Created by 1 on 5/1/2016.
 */
public class Copier implements Runnable {

    private final String fileName;
    private File rootFolder;

    public Copier(String path, String fileName) {
        this.rootFolder = new File(path);
        this.fileName = fileName;
    }

    @Override
    public void run() {
        copyFiles();
    }


    private void copyFiles() {

        if (!rootFolder.exists()) {
            out.println("Folder doesn't exist");
            return;
        }
        out.println("Folder exist");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        searchInFolder(rootFolder);

    }

    private void searchInFolder(File rootFolder) {
        for (File desiredFile : rootFolder.listFiles()) {

            if (desiredFile.isDirectory()) {
                searchInFolder(desiredFile);
                continue;
            }

            if (desiredFile.getName().contains(fileName)) {
                renameFile(desiredFile);

            }
        }
    }

    private void renameFile(File file) {
        String newFilePath = file.getName();
        renameParent(file.getParentFile(), newFilePath, file);
    }

    private void renameParent(File parentFile, String newFilePath, File oldFile) {
        String newFolderName = parentFile.getName() + /*Time.valueOf(LocalTime.now())*/ /*+BaseCalendar.Date.TIME_UNDEFINED _*/  returnSeconds(1970, 01, 01) + File.separator + newFilePath;
        out.println(newFolderName);
        if (!parentFile.getPath().equals(rootFolder.getPath())) {
            renameParent(parentFile.getParentFile(), newFolderName, oldFile);
            return;
        }

        copyNewFile(parentFile.getParentFile().getAbsolutePath() + newFolderName, oldFile);
    }

    public long returnSeconds(int year, int month, int date) {
        Calendar calendarDes = Calendar.getInstance();
        calendarDes.set(year, month, date);
        long milliseconds = calendarDes.getTimeInMillis();
        return milliseconds;
    }

    private void copyNewFile(String newPath, File oldFile) {
        File newTargetFile = new File(newPath);
        out.println(newTargetFile.getAbsolutePath());
        newTargetFile.getParentFile().mkdirs();
        try {
            Files.copy(oldFile.toPath(), newTargetFile.toPath());
        } catch (IOException e) {
            e.getStackTrace();
        }
    }
}
