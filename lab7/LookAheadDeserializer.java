package lab7;

/**
 * Created by 1 on 5/11/2016.
 */
import java.io.*;

public class LookAheadDeserializer {

    private static byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(obj);
        byte[] buffer = baos.toByteArray();
        oos.close();
        baos.close();
        return buffer;
    }

    private static Object deserialize(byte[] buffer) throws IOException,
            ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(buffer);

        // Используем LookAheadObjectInputStream вместо InputStream
        ObjectInputStream ois = new LookAheadObjectInputStream(bais);

        Object obj = ois.readObject();
        ois.close();
        bais.close();
        return obj;
    }

    public static void main(String[] args) {
        try {
            // Сериализация экземпляра Bicycle
            byte[] serializedBicycle = serialize(new Bicycle(0, "Unicycle", 1));

            // Сериализация экземпляра File
            byte[] serializedFile = serialize(new File("infoin.txt"));

            // Десериализация экземпляра Bicycle (легитимный случай использования)
            Bicycle bicycle0 = (Bicycle) deserialize(serializedBicycle);
            System.out.println(bicycle0.getName() + " has been deserialized.");

            // Десериализация экземпляра File (ошибка)
            Bicycle bicycle1 = (Bicycle) deserialize(serializedFile);

        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }
}