package lab4;

import static java.lang.System.out;

/**
 * Created by andriygorbach on 4/12/2016.
 * 1ый - если скорость автомобиля на момент поворота больше половины от максимальной - то он получает +0,5% от разницы между половиной от максимальной и нынешней к маневренности на этот
 * поворот следующим образом (половина максимальной = 75. Нынешняя равна 90. Разница = 15. Маневренность до срабатывания = 0,5, стала = 0,575)
 */

public class BMW extends Cars {


    public BMW (  double vmax, double accel, double manevr ) {
        this.vmax = vmax;
        this.accel = accel;
        this.accel_const = accel;
        this.manevr = manevr;
        this.v0 = 0;
        this.vtek = 0;
        this.t = 0;
    }


    public void povorot () {
        if (vtek > vmax / 2) {
            v0 = (manevr + 0.05 * (vmax / 2 - vtek)) * vtek;
        } else {
            v0 = vtek * manevr;
        }
    }

}

