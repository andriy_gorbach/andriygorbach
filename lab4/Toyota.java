package lab4;

/**
 * Created by andriygorbach on 4/12/2016.
 * 2ой - если скорость автомобиля после поворота меньше чем половина от максимальной то его ускорение на этом отрезке увеличивается в 2 раза
 */
public class Toyota extends Cars {

    public Toyota  (  double vmax, double accel, double manevr ) {
        this.vmax = vmax;
        this.accel = accel;
        this.accel_const = accel;
        this.manevr = manevr;
        this.v0 = 0;
        this.vtek = 0;
        this.t = 0;
    }


    public void povorot () {
        if (v0 < vmax / 2) {
            if (accel == accel_const) {
                accel = accel_const * 2;
            }
        } else if (accel != accel_const) {
            accel = accel_const;
        }

        v0 = vtek * manevr;
    }
}
