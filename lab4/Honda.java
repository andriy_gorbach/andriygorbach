package lab4;

/**
 * Created by andriygorbach on 4/12/2016.
 * 3ий - если автомобиль в момент поворота достиг своей максимальной скорости, то его максимальная скорость увеличивается на 10% до конца гонки
 */
public class Honda extends Cars {

    public Honda (  double vmax, double accel, double manevr ) {
        this.vmax = vmax;
        this.accel = accel;
        this.accel_const = accel;
        this.manevr = manevr;
        this.v0 = 0;
        this.vtek = 0;
        this.t = 0;
    }


    public void povorot () {
        if (vtek == vmax) {
            vmax += vmax * 0.1;
        }
        v0 = vtek * manevr;
    }



}

