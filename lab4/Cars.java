package lab4;

/**
 * Created by andriygorbach on 4/12/2016.
 */

public abstract class Cars {
    public double t;
    public double v0;
    public double vmax;
    public double vtek;
    public double accel;
    public double accel_const;
    public double manevr;
    public double mobility;
    public double s = 2000;
    public double s0 = 0;


    public void dvighenie () {
        vtek = Math.sqrt(Math.pow(v0, 2) + (accel * 2 * s));
        if (vtek > vmax) {
            s0 = ((Math.pow(vmax, 2) - Math.pow(v0, 2)) / (2 * accel));
            t += (vmax - v0) / accel;
            t += ((s - s0) / vmax);
        } else {
            t += (vtek - v0) / accel;
        }
    }


    public abstract void povorot ();
  }
