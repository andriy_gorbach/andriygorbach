package some_name;

/**
 * Created by andriygorbach on 4/12/2016.
 * При помощи цикла for вывести на экран нечетные числа от 1 до 99.
 */
public class class_6 {

    public static void main ( String[] args ) {
        // int c = 100;
        int[] array = new int[101];
        for (int c = 0; c < 101; c++) {
            for (array[c] = c; array[c] % 2 != 0; c++)
                System.out.println("Odd number: " + c);

        }
    }
}