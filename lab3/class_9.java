package some_name;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by andriygorbach on 4/12/2016.
 * Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма». Вывести на экран полученную сумму.
 */
public class class_9 {


    public static void main ( String[] args ) throws IOException {
        BufferedReader q = new BufferedReader(new InputStreamReader(System.in));

        int ou = 0;
        boolean b = true;
        String s = "сумма";


        System.out.println("Populate numbers. Sum of numbers will be counted until 'сумма' word be populated:");
        try {
            while (b) {
                String summa = q.readLine();

                if (!summa.equals(s)) {
                    int au = Integer.parseInt(summa);
                    ou += au;
                    System.out.println("Addition of previously populated number with " + au + " is " + ou);
                } else break;

            }
            System.out.println("happened due to 'сумма' word was populated");
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid. Please, insert only integer numbers");
        }
    }
}