package some_name;

/**
 * Created by andriygorbach on 4/12/2016.
 * Дан массив из 4 чисел типа int. Вывести их все.
 */
public class class_8 {

    public static void main ( String[] args ) {

        int[] a = {1, 15, -9, 789};
            System.out.println("All array elements are:");
        for (int x = 0; x < a.length; x++) {
            System.out.println(a[x]);
        }

    }
}
