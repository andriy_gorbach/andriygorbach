package some_name;

/**
 * Created by andriygorbach on 4/12/2016.
 * Дан массив из 4 чисел типа int. Сравнить их и вывести наименьшее на консоль.
 */
public class class_5 {

    public static void main ( String[] args ) {

        int[] a = {1, 15, -9, 789};
        int Min = a[0];
        for (int x : a) {
            if (x < Min) Min = x;
        }
        System.out.print(Min);
    }

}

