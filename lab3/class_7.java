package some_name;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by andriygorbach on 4/12/2016.
 * Вывести все простые числа от 1 до N. N задается пользователем через консоль
 */


public class class_7 {
    public static BufferedReader q = new BufferedReader(new InputStreamReader(System.in));

    public static void main ( String[] args ) throws IOException {


        System.out.println("Populate an integer number");

        String num = q.readLine();
        int n2 = Integer.parseInt(num);
        if ((num.equals("0")) || (num.equals("1"))) {
            System.out.println("0 and 1 are nor simple nor complicated numbers. Try again");
        }

        int a, b = 0;
        System.out.println("Simple numbers from 1 to " + num + " are");
        try {
            for (a = 2; a < n2; a++) {
                // b = 0;
                for (int i = 1; i <= a; i++) {
                    if (a % i == 0)
                        b++;
                }
                //  if (b <= 2)
                System.out.println(a);
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid. Please, insert only integer numbers");

        }
    }
}